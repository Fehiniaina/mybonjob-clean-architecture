# Executables (local)
DOCKER_COMP = @cd docker && docker-compose

# Docker containers
EXEC_PHP = $(DOCKER_COMP) exec -T php

EXEC_CONTAINER = @cd docker && docker exec -it
# Executables
PHP      = $(EXEC_PHP) php
COMPOSER = $(EXEC_PHP) composer
SYMFONY  = $(EXEC_PHP) bin/console

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Symfony-docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) build

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

sh: ## Connect to the PHP FPM container
	@$(eval container ?=)
	@$(eval type ?=)
	@$(EXEC_CONTAINER) $(container) $(type)


## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-dev --no-progress --no-scripts --no-interaction
vendor: composer

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf

##################
# CI (workplace container)
##################
.PHONY: cs_check
cs_check: ## Connect to the PHP FPM container
	@$(EXEC_PHP) php-cs-fixer fix --config=.php-cs-fixer.php -v --allow-risky=yes --dry-run --diff --stop-on-violation

.PHONY: cs_fix
cs_fix:
	@$(EXEC_PHP) php-cs-fixer fix --config=.php-cs-fixer.php -v

.PHONY: phpunit
phpunit:
	@$(EXEC_PHP) bin/phpunit

.PHONY: deptrac
deptrac:
	@$(EXEC_PHP) deptrac analyze deptrac.yaml

analyze: deptrac cs_check cs_fix phpunit

.PHONY: phpcs
phpcs: phpcs-testsuite
phpcs-testsuite:
	@$(EXEC_PHP) vendor/bin/phpcs -p --colors --extensions=php --standard=PSR12  src

.PHONY: phpcs-build
phpcs-build: phpcs-testsuite
phpcs-build-testsuite:
	@$(EXEC_PHP) vendor/bin/phpcbf -p --colors --extensions=php --standard=PSR12  src

phpunit-coverage: phpunit-coverage-testsuite
phpunit-coverage-testsuite:
	rm -rf build/test
	#@todo ./php does not work here
	docker exec -it php bash -c "\
		export XDEBUG_MODE=coverage && \
		vendor/bin/phpunit \
			--exclude-group='disabled' \
			--log-junit build/test/phpunit/junit.xml \
			--coverage-html build/test/phpunit tests \
		unset XDEBUG_MODE";
	@printf "\n${ORANGE}--> ${NC}${GREEN}Coverage report build at path:${NC} build/test/phpunit\n\n";

## —— Consumer  ————————————————————————————————————————————————————————

supervisord:
	docker exec -it php supervisord --configuration /etc/supervisor.d/supervisord.ini

## —— RUN  ————————————————————————————————————————————————————————————
tests: \
	phpcs-testsuite
