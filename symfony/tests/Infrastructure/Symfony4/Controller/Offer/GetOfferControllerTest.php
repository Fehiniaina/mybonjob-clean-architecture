<?php

namespace Tests\Infrastructure\Symfony4\Controller\Offer;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class GetOfferControllerTest
 * @package Tests\Infrastructure\Symfony4\Controller\Offer
 */
class GetOfferControllerTest extends WebTestCase
{
    /** @var KernelBrowser|null  */
    private $client = null;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @test
     */
    public function itShouldShowOfferWithoutParams(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/offers');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
    }

    /**
     * @test
     */
    public function itShouldShowOfferWithParams(): void
    {
        $parameters = [
            'page' => [
                'number' => FakerMother::random()->numberBetween(1, 10),
                'size' => FakerMother::random()->numberBetween(10, 50),
            ],
            'sort' => 'id'
        ];
        $this->client->request('GET', '/api/offers', $parameters);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
}
