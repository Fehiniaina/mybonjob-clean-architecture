<?php

namespace Tests\Infrastructure\Symfony4\Controller\Offer;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class CreateOfferControllerTest
 * @package Tests\Infrastructure\Symfony4\Controller\Offer
 */
class CreateOfferControllerTest extends WebTestCase
{
    /** @var KernelBrowser|null  */
    private $client = null;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->client->catchExceptions(false);
    }

    /**
     * @test
     * @expectedException UnsupportedMediaTypeHttpException
     */
    public function itShouldCreateOfferAssertException(): void
    {
        $this->expectException(UnsupportedMediaTypeHttpException::class);
        $content = [
            'title' => FakerMother::random()->title(),
            'description' => FakerMother::random()->realText(),
            'status' => false,
        ];
        $this->client->request(
            Request::METHOD_POST,
            '/api/offers',
            [],
            [],
            [],
            json_encode($content)
        );
    }

    /**
     * @test
     * @expectedException HandlerFailedException
     */
    public function itShouldCreateOfferAssertFailure(): void
    {
        $this->expectException(HandlerFailedException::class);
        $content = [
            'title' => FakerMother::random()->paragraph(),
            'description' => FakerMother::random()->realText(),
            'status' => false,
        ];
        $this->client->jsonRequest(Request::METHOD_POST, '/api/offers', $content);
    }

    /**
     * @test
     */
    public function itShouldCreateOfferAssertSuccess(): void
    {
        $content = [
            'title' => FakerMother::random()->title(),
            'description' => FakerMother::random()->realText(),
            'status' => false,
        ];
        $this->client->jsonRequest(Request::METHOD_POST, '/api/offers', $content);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
}
