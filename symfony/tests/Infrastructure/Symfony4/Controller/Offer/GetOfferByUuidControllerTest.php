<?php

namespace Tests\Infrastructure\Symfony4\Controller\Offer;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GetOfferByUuidControllerTest
 * @package Tests\Infrastructure\Symfony4\Controller\Offer
 */
class GetOfferByUuidControllerTest extends WebTestCase
{
    /** @var KernelBrowser|null  */
    private $client = null;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @test
     */
    public function itShouldShowOfferByUuid(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/offers');
    }
}
