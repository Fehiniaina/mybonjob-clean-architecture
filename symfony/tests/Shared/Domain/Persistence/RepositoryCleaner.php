<?php

namespace Tests\Shared\Domain\Persistence;

/**
 * Interface RepositoryCleaner
 * @package Seat\Tests\Domain\Persistence
 */
interface RepositoryCleaner
{
    public function truncateDataStored(): void;
}
