<?php

namespace Tests\Shared\Application;

use Seat\Shared\Application\Criteria;
use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class CriteriaMother
 * @package Tests\Shared\Application
 */
final class CriteriaMother
{
    public static function create(
        ?array $exp = null,
        ?array $sort = null,
        ?int $page = null,
        ?int $size = null
    ): Criteria {
        return new Criteria(
            $exp ?? [],
            $sort ?? [],
            $page ?? FakerMother::random()->numberBetween(1, 10),
            $size ?? FakerMother::random()->numberBetween(1, 10)
        );
    }
}
