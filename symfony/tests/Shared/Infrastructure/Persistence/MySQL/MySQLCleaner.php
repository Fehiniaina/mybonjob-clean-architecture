<?php

namespace Tests\Shared\Infrastructure\Persistence\MySQL;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Tests\Shared\Domain\Persistence\RepositoryCleaner;

/**
 * Class MySQLCleaner
 * @package Seat\Tests\Infrastructure\Persistence\MySQL
 */
final class MySQLCleaner implements RepositoryCleaner
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * MySQLCleaner constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function truncateDataStored(): void
    {
        $connection = $this->entityManager->getConnection();
        $tables = $this->tables($connection);
        $truncateTablesSql = $this->truncateDatabaseSql($tables);
        $connection->executeStatement($truncateTablesSql);
    }

    /**
     * @param Connection $connection
     * @return array
     * @throws \Doctrine\DBAL\Exception
     */
    private function tables(Connection $connection): array
    {
        return $connection->executeQuery('SHOW TABLES')->fetchAllNumeric();
    }

    /**
     * @param array $tables
     * @return string
     */
    private function truncateDatabaseSql(array $tables): string
    {
        $sql = array_map(static function ($table): string {
            return ($table[0] === 'doctrine_migration_versions') ? '' : sprintf('TRUNCATE TABLE `%s`;', $table[0]);
        }, $tables);

        return sprintf('SET FOREIGN_KEY_CHECKS = 0; %s SET FOREIGN_KEY_CHECKS = 1;', implode(' ', $sql));
    }
}
