<?php

namespace Tests\Shared\Infrastructure\PhpUnit;

use Faker\Factory;
use Faker\Generator;

/**
 * Class FakerMother
 * @package Seat\Tests\Infrastructure\PhpUnit
 */
final class FakerMother
{
    private static $faker;

    public static function random(): Generator
    {
        return self::$faker = self::$faker ?? Factory::create();
    }
}
