<?php

namespace Tests\Seat\Offer\Domain;

use Seat\Api\Offer\Domain\OfferCreatedDomainEvent;

/**
 * Class OfferCreatedDomainEventMother
 * @package Tests\Seat\Offer\Domain
 */
final class OfferCreatedDomainEventMother
{
    /**
     * @param string $id
     * @param string $title
     * @param string $description
     * @param bool $status
     * @return OfferCreatedDomainEvent
     * @throws \Exception
     */
    public static function create(
        string $id,
        string $title,
        string $description,
        bool $status
    ): OfferCreatedDomainEvent {
        return new OfferCreatedDomainEvent(
            $id,
            $title,
            $description,
            $status
        );
    }
}
