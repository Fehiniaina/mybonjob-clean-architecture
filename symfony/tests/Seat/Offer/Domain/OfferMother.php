<?php

namespace Tests\Seat\Offer\Domain;

use Seat\Api\Offer\Domain\Entities\Model\OfferModel;

/**
 * Class OfferMother
 * @package Tests\Seat\Offer\Domain
 */
final class OfferMother
{
    public static function create(
        ?OfferIdMother $id = null,
        ?OfferTitleMother $title = null
    ): OfferModel {
        return OfferModel::create(
          $id ?? OfferIdMother::create(),
            $title ?? OfferTitleMother::create(),
            OfferTitleMother::create(),
            true
        );
    }
}
