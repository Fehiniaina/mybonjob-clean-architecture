<?php

namespace Tests\Seat\Offer\Domain;

use Seat\Api\Offer\Domain\Entities\Model\OfferModel;
use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class OfferFieldsMother
 * @package Tests\Seat\Offer\Domain
 */
final class OfferFieldsMother
{
    public static function create($fields = []): ?array
    {
        if (count($fields) !== 0) {
            return $fields;
        }

        $result = [];
        $albumFields = OfferModel::getFieldsName();
        $total = FakerMother::random()->numberBetween(1, count($albumFields));
        for ($i = 0; $i < $total; $i++) {
            $field = FakerMother::random()->numberBetween(0, count($albumFields) - 1);
            $result[] = $albumFields[$field];
            unset($albumFields[$field]);
            $albumFields = array_values($albumFields); // reindex
        }

        return $result;
    }
}
