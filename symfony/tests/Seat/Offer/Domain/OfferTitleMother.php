<?php

namespace Tests\Seat\Offer\Domain;

use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class OfferTitleMother
 * @package Seat\Tests\Offer\Domain
 */
final class OfferTitleMother
{
    public static function create(): string
    {
        return FakerMother::random()->word();
    }
}
