<?php

namespace Tests\Seat\Offer\Domain;

use Seat\Api\Offer\Domain\OfferId;
use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class OfferIdMother
 * @package Seat\Tests\Offer\Domain
 */
final class OfferIdMother
{
    public static function create(): OfferId
    {
        return new OfferId(FakerMother::random()->uuid());
    }
}
