<?php

namespace Tests\Seat\Offer\Application\Add;

use Seat\Api\Offer\Domain\Entities\Model\OfferModel;
use Seat\Api\Offer\Domain\OfferId;
use Tests\Seat\Offer\Domain\OfferIdMother;
use Tests\Seat\Offer\Domain\OfferTitleMother;

/**
 * Class PostOfferCommandCreator
 * @package Seat\Tests\Offer\Application\Add
 */
final class PostOfferCommandCreator
{
    /**
     * @param OfferId $id
     * @param string $title
     * @param string $description
     * @param bool $status
     * @return OfferModel
     * @throws \Exception
     */
    public static function create(
        OfferId $id = null,
        string $title = null,
        string $description = null,
        bool $status = true
    ): OfferModel {
        return OfferModel::create(
            $id ?? OfferIdMother::create(),
            $title ?? OfferTitleMother::create(),
            $description ?? OfferTitleMother::create(),
            $status
        );
    }
}
