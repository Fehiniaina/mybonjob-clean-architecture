<?php

namespace Tests\Seat\Offer\Application\Add;

use Seat\Api\Offer\Application\AddOffer\AddOffer;
use Seat\Api\Offer\Application\AddOffer\AddOfferRequest;
use Seat\Api\Offer\Domain\Entities\Model\OfferModel;
use Seat\Api\Offer\Domain\OfferCreatedDomainEvent;
use Seat\Api\Offer\Domain\OfferRepository;
use Seat\Shared\Domain\Bus\Event\EventPublisher;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\Seat\Offer\Domain\OfferCreatedDomainEventMother;

/**
 * Class OfferCreateTest
 * @package Seat\Tests\Offer\Application\Add
 */
final class OfferCreateTest extends KernelTestCase
{
    /**
     * @test
     * @throws \Exception
     */
    public function itShouldOfferCreate(): void
    {
        $offer = PostOfferCommandCreator::create();
        $eventExpected = OfferCreatedDomainEventMother::create(
            $offer->id(),
            $offer->title(),
            $offer->description(),
            $offer->status()
        );
        $repository = \Mockery::mock(OfferRepository::class);
        $publisher = \Mockery::mock(EventPublisher::class);
        $this->assertNotEmpty($offer->dropEvents());

        $repository->expects()->add(
            \Mockery::on(
                $this->compareOffers($offer)
            )
        );

        $publisher->expects()->publish(
            \Mockery::on(
                $this->compareEvents($eventExpected)
            )
        );

        $addOffer = new AddOffer($repository, $publisher);
        $addOffer->execute(AddOfferRequest::fromAll(
            $offer->title(),
            $offer->description(),
            $offer->status(),
            $offer->id()
        ));
    }

    private function compareOffers(OfferModel $expected): callable
    {
        return static function ($actual) use ($expected): bool {
            return $expected->equals($actual);
        };
    }

    private function compareEvents(OfferCreatedDomainEvent $expected): callable
    {
        /** @var OfferCreatedDomainEvent $expected
         * @return bool
         */
        return static function ($actual) use ($expected): bool {
            return $expected->aggregateId() === $actual->aggregateId() &&
                $expected->getTitle() === $actual->getTitle() &&
                $expected->getDescription() === $actual->getDescription() &&
                $expected->getStatus() === $actual->getStatus();
        };
    }
}
