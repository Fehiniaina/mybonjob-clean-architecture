<?php

namespace Tests\Seat\Offer\Application\Get\Collection;

use PHPUnit\Framework\TestCase;
use Seat\Api\Offer\Application\ShowOffer\Collections\ShowOfferResponse;
use Seat\Api\Offer\Application\ShowOffer\ShowOffer;
use Seat\Api\Offer\Domain\OfferAssembler;
use Seat\Api\Offer\Domain\OfferRepository;
use Seat\Api\Offer\Domain\OfferResultSet;
use Seat\Api\Offer\Infrastructure\Doctrine\DoctrineOfferRepository;
use Tests\Seat\Offer\Domain\OfferMother;
use Tests\Shared\Application\CriteriaMother;

/**
 * Class OfferSearcherTest
 * @package Tests\Seat\Offer\Application\Get\Collection
 */
class OfferSearcherTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldSearchOffersAsArray(): void
    {
        $query = GetOffersQueryMother::create();

        $offer1 = OfferMother::create();
        $offer2 = OfferMother::create();
        $offer3 = OfferMother::create();

        $offers = [
            OfferAssembler::fromEntityToArray($offer1),
            OfferAssembler::fromEntityToArray($offer2),
            OfferAssembler::fromEntityToArray($offer3),
        ];

        CriteriaMother::create(
            $query->getFilters(),
            $query->getSort()
        );

        SelectMother::create(
            $query->getFilters()
        );

        $resultSet = new OfferResultSet($offers, \count($offers));

        $repository = \Mockery::mock(DoctrineOfferRepository::class);
        $repository->allows()->getMatching(\Mockery::any(), \Mockery::any())->andReturns($resultSet);

        $expected = new ShowOfferResponse(
            $resultSet->getOffers(),
            $resultSet->getTotal(),
            $query->getPage(),
            $query->getSize()
        );

        $actualResponse = (new ShowOffer($repository))->execute($query);

        self::assertEquals($expected, $actualResponse);
    }

    /**
     * @test
     */
    public function itShouldSearchOffersAsEntity(): void
    {
        $query = GetOffersQueryMother::create();

        $offer1 = OfferMother::create();
        $offer2 = OfferMother::create();
        $offer3 = OfferMother::create();

        $offers = [$offer1, $offer2, $offer3];

        CriteriaMother::create(
            $query->getFilters(),
            $query->getSort()
        );

        SelectMother::create(
            $query->getFilters()
        );

        $resultSet = new OfferResultSet($offers, \count($offers));

        $repository = \Mockery::mock(OfferRepository::class);
        $repository->allows()->getMatching(\Mockery::any(), \Mockery::any())->andReturns($resultSet);

        $expected = new ShowOfferResponse(
            $resultSet->getOffers(),
            $resultSet->getTotal(),
            $query->getPage(),
            $query->getSize()
        );

        $actualResponse = (new ShowOffer($repository))->execute($query);

        self::assertEquals($expected, $actualResponse);
        self::assertEquals($expected->toJson(), $actualResponse->toJson());
    }

    /**
     * @test
     */
    public function itShouldSearchOfferAsEmpty(): void
    {
        $query = GetOffersQueryMother::create();

        $offers = [];

        CriteriaMother::create(
            $query->getFilters(),
            $query->getSort()
        );

        SelectMother::create(
            $query->getFilters()
        );

        $resultSet = new OfferResultSet($offers, \count($offers));

        $repository = \Mockery::mock(OfferRepository::class);
        $repository->allows()->getMatching(\Mockery::any(), \Mockery::any())->andReturns($resultSet);

        $expected = new ShowOfferResponse(
            $resultSet->getOffers(),
            $resultSet->getTotal(),
            $query->getPage(),
            $query->getSize()
        );

        $actualResponse = (new ShowOffer($repository))->execute($query);

        self::assertEquals($expected, $actualResponse);
        self::assertEquals($expected->toJson(), $actualResponse->toJson());
    }
}
