<?php

namespace Tests\Seat\Offer\Application\Get\Collection;

use Seat\Api\Offer\Application\ShowOffer\Collections\ShowOfferQuery;
use Tests\Seat\Offer\Domain\OfferFieldsMother;
use Tests\Shared\Infrastructure\PhpUnit\FakerMother;

/**
 * Class GetOffersQueryTest
 * @package Tests\Seat\Offer\Application\Get\Collection
 */
final class GetOffersQueryMother
{
    public static function create(
        ?int $page = null,
        ?int $size = null,
        ?array $fields = null,
        ?array $filters = null,
        ?array $sort = null
    ): ShowOfferQuery {
        return new ShowOfferQuery(
            $page ?? FakerMother::random()->numberBetween(1, 10),
            $size ?? FakerMother::random()->numberBetween(1, 10),
            $fields ?? OfferFieldsMother::create(),
            $filters ?? [],
            $sort ?? array_map(static function($field): string {
                return FakerMother::random()->randomElement(['', '-']). $field;
            }, OfferFieldsMother::create())
        );
    }
}
