<?php

namespace Tests\Seat\Offer\Application\Get\Collection;

use Seat\Api\Offer\Domain\OfferAssembler;
use Seat\Shared\Domain\Select;

/**
 * Class SelectMother
 * @package Tests\Seat\Offer\Application\Get\Collection
 */
final class SelectMother
{
    public static function create(
        ?array $fields = [],
        ?string $alias = 'a',
        ?bool $fetchArray = false
    ): Select {
        return new Select(
            $fields ?? array_keys(OfferAssembler::$jsonMappingToEntity),
            $alias,
            $fetchArray
        );
    }
}
