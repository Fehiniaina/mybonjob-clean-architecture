<?php

namespace Seat\Shared\Service;

/**
 * Interface ApplicationService
 * @package Seat\Shared\Service
 */
interface ApplicationService
{
    /**
     * @param Request $request
     * @return Response|null
     */
    public function execute(Request $request): ?Response;
}
