<?php

namespace Seat\Shared\Service;

/**
 * Interface PasswordHasherInterface
 * @package Seat\Shared\Service
 */
interface PasswordHasherInterface
{
    public function hash(string $plainPassword): string;
}
