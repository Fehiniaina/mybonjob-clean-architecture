<?php

namespace Seat\Shared\Service;

/**
 * Class PasswordEncoder
 * @package Seat\Shared\Service
 */
final class PasswordEncoder implements PasswordHasherInterface
{
    public function hash(string $plainPassword): string
    {
        return password_hash($plainPassword, PASSWORD_ARGON2I);
    }
}
