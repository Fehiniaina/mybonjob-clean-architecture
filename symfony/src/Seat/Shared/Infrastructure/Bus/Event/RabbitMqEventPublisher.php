<?php

namespace Seat\Shared\Infrastructure\Bus\Event;

use Seat\Shared\Domain\Bus\Event\EventPublisher;
use Seat\Shared\Domain\DomainEvent;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class RabbitMqEventPublisher
 * @package Seat\Shared\Application\Bus\Event
 */
final class RabbitMqEventPublisher implements EventPublisher
{
    /** @var MessageBusInterface */
    private $eventBus;

    /**
     * RabbitMqEventPublisher constructor.
     * @param MessageBusInterface $eventBus
     */
    public function __construct(MessageBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * @param DomainEvent ...$events
     */
    public function publish(DomainEvent ...$events): void
    {
        foreach ($events as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}
