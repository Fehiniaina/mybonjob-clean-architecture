<?php

namespace Seat\Shared\Infrastructure\Bus;

use Seat\Shared\Domain\Bus\BusHandler;
use Seat\Shared\Domain\Bus\Command\Command;
use Seat\Shared\Service\Request;
use Seat\Shared\Service\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

/**
 * Class SymfonyMessengerHandler
 * @package Seat\Shared\Infrastructure\Bus
 */
final class SymfonyMessengerHandler implements BusHandler
{
    /** @var MessageBusInterface */
    private $defaultBus;
    /** @var MessageBusInterface */
    private $queryBus;
    /** @var MessageBusInterface */
    private $commandBus;

    /**
     * SymfonyMessengerHandler constructor.
     * @param MessageBusInterface $defaultBus
     * @param MessageBusInterface $queryBus
     * @param MessageBusInterface $commandBus
     */
    public function __construct(
        MessageBusInterface $defaultBus,
        MessageBusInterface $queryBus,
        MessageBusInterface $commandBus
    ) {
        $this->defaultBus = $defaultBus;
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    /**
     * @param Request $request
     * @return Response|null
     */
    public function dispatch(Request $request): ?Response
    {
        switch (true) {
            case $request instanceof Command:
                $envelope = $this->commandBus->dispatch($request);
                break;
            case $request instanceof Request:
                $envelope = $this->queryBus->dispatch($request);
                break;
            default:
                $envelope = $this->defaultBus->dispatch($request);
                break;
        }

        $stamp = $envelope->last(HandledStamp::class);

        return $stamp ? $stamp->getResult() : null;
    }
}
