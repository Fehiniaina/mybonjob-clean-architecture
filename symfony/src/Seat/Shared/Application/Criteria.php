<?php

namespace Seat\Shared\Application;

use Doctrine\Common\Collections\Criteria as CriteriaDoctrine;
use Doctrine\Common\Collections\Expr\Expression;

/**
 * Class Criteria
 * @package Seat\Shared\Application
 */
final class Criteria
{
    /** @var CriteriaDoctrine */
    private $criteria;
    /** @var string */
    private $alias;

    public function __construct(
        array $exp,
        array $sort,
        int $page,
        int $size,
        string $alias = 'o'
    ) {
        $page = ($page - 1) * $size;
        $this->alias = $alias;
        $this->criteria = new CriteriaDoctrine($this->buildExp($exp), $this->buildSort($sort), $page, $size);
    }

    public function getCriteria(): CriteriaDoctrine
    {
        return $this->criteria;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    private function buildExp(array $exp): ?Expression
    {
        if ($exp === []) {
            return null;
        }

        // @todo filters here
        return CriteriaDoctrine::expr()->contains('', []);
    }

    /**
     * @param array $sort
     * @return array
     */
    private function buildSort(array $sort): array
    {
        $results = [];
        foreach ($sort as $field) {
            $order = ($field[0] === '-' ? CriteriaDoctrine::DESC : CriteriaDoctrine::ASC);
            $name = ($field[0] === '-' ? substr($field, 1, \strlen($field)) : $field);
            $results[$name] = $order;
        }

        return $results;
    }
}
