<?php

namespace Seat\Shared\Domain\ValueObject;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Uuid
 * @package Seat\Shared\Domain\ValueObject
 */
class UuidValueObject extends Uuid implements \Stringable
{
    private static $constrains = [];

    public function __toString(): string
    {
        return $this->uid;
    }

    public function value(): string
    {
        return $this->uid;
    }

    /**
     * @return mixed[]
     */
    public static function getConstraints(): array
    {
        if (!isset(self::$constrains)) {
            self::$constrains = [new Assert\NotBlank(), new Assert\Uuid()];
        }
        return self::$constrains;
    }

    public static function random(): self
    {
        return new static(Uuid::v4()->toRfc4122());
    }
}
