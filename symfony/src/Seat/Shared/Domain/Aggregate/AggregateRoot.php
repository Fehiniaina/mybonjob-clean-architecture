<?php

namespace Seat\Shared\Domain\Aggregate;

use Seat\Shared\Domain\DomainEvent;

/**
 * Class AggregateRoot
 * @package Seat\Shared\Domain\Aggregate
 */
abstract class AggregateRoot
{
    private $domainEvents = [];

    final public function dropEvents(): array
    {
        $domainEvents = $this->domainEvents;
        $this->domainEvents = [];

        return $domainEvents;
    }

    abstract public function equals($object): bool;

    final protected function collectEvent(DomainEvent $domainEvent): void
    {
        $this->domainEvents[] = $domainEvent;
    }

    final protected function countEvents(): int
    {
        return \count($this->domainEvents);
    }
}
