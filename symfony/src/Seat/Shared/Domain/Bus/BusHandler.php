<?php

namespace Seat\Shared\Domain\Bus;

use Seat\Shared\Service\Request;
use Seat\Shared\Service\Response;

/**
 * Interface BusHandler
 * @package Seat\Shared\Domain\Bus
 */
interface BusHandler
{
    public function dispatch(Request $request): ?Response;
}
