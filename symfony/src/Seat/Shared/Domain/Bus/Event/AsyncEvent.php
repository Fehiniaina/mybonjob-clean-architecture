<?php

namespace Seat\Shared\Domain\Bus\Event;

/**
 * Interface AsyncEvent
 * @package Seat\Shared\Domain\Bus\Event
 */
interface AsyncEvent extends Event
{
}
