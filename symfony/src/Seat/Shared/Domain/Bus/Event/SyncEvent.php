<?php

namespace Seat\Shared\Domain\Bus\Event;

/**
 * Interface SyncEvent
 * @package Seat\Shared\Domain\Bus\Event
 */
interface SyncEvent extends Event
{
}
