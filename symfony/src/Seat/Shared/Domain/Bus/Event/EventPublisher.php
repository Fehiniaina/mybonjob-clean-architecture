<?php

namespace Seat\Shared\Domain\Bus\Event;

use Seat\Shared\Domain\DomainEvent;

/**
 * Interface EventPublisher
 * @package Seat\Shared\Domain\Bus\Event
 */
interface EventPublisher
{
    public function publish(DomainEvent ...$events): void;
}
