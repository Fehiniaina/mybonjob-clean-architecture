<?php

namespace Seat\Shared\Domain\Bus\Query;

/**
 * Interface AsyncQuery
 * @package Seat\Shared\Domain\Query
 */
interface SyncQuery extends Query
{
}
