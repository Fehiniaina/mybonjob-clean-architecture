<?php

namespace Seat\Shared\Domain\Bus\Query;

use Seat\Shared\Service\Request;

/**
 * Interface Query
 * @package Seat\Shared\Domain\Query
 */
interface Query extends Request
{
}
