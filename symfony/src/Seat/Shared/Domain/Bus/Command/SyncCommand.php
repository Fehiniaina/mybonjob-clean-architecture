<?php

namespace Seat\Shared\Domain\Bus\Command;

/**
 * Interface SyncCommand
 * @package Seat\Shared\Domain\Bus\Command
 */
interface SyncCommand extends Command
{
}
