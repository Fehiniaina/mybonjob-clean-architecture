<?php

namespace Seat\Shared\Domain\Bus\Command;

use Seat\Shared\Service\Request;

/**
 * Interface Command
 * @package Seat\Shared\Domain\Bus
 */
interface Command extends Request
{
}
