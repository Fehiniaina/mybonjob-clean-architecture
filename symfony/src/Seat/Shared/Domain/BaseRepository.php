<?php

namespace Seat\Shared\Domain;

use Seat\Shared\Domain\Aggregate\AggregateRoot;

/**
 * Interface BaseRepository
 * @package Seat\Shared\Domain
 */
interface BaseRepository
{
    /**
     * @param AggregateRoot $aggregate
     */
    public function add(AggregateRoot $aggregate): void;

    /**
     * @param string $id
     * @return AggregateRoot
     */
    public function findById(string $id): AggregateRoot;

    /**
     * @param array $criteria
     * @return AggregateRoot[]
     */
    public function findByCriteria(array $criteria): array;
}
