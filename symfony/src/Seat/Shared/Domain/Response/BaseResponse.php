<?php

namespace Seat\Shared\Domain\Response;

use Seat\Shared\Domain\Error\Notification;

/**
 * Class BaseResponse
 * @package Seat\Shared\Domain\Response
 */
abstract class BaseResponse
{
    /** @var Notification */
    protected $notification;

    public function __construct()
    {
        $this->notification = new Notification();
    }

    public function notification(): Notification
    {
        return $this->notification;
    }

    public function addError(string $fieldName, string $error)
    {
        $this->notification->addError($fieldName, $error);
    }

    public function addSuccess(string $type, string $message)
    {
    }
}
