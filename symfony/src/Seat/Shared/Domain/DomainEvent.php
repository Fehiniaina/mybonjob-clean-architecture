<?php

namespace Seat\Shared\Domain;

use Symfony\Component\Uid\Uuid;

/**
 * Class DomainEvent
 * @package Seat\Shared\Domain
 */
abstract class DomainEvent
{
    public static $dateFormat = 'Y-m-d H:i:s';
    private $aggregateId;
    private $eventId;
    private $eventDate;

    /**
     * DomainEvent constructor.
     * @param string $aggregateId
     * @param string|null $eventId
     * @param string|null $eventDate
     * @throws \Exception
     */
    public function __construct(string $aggregateId, string $eventId = null, string $eventDate = null)
    {
        $this->aggregateId = $aggregateId;
        $this->eventId = $eventId ?: Uuid::v4()->toRfc4122();
        $this->eventDate = $eventDate ?: (new \DateTimeImmutable())->format(self::$dateFormat);
    }

    abstract public static function fromPrimitives(
        string $aggregateId,
        array $data,
        string $eventId,
        string $eventDate
    ): self;

    /**
     * @return mixed[]
     */
    abstract public function toPrimitives(): array;

    abstract public static function eventName(): string;

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function occurredOn(): string
    {
        return $this->eventDate;
    }
}
