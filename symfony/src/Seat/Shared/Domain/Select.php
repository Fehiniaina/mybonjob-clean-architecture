<?php

namespace Seat\Shared\Domain;

/**
 * Class Select
 * @package Seat\Shared\Domain
 */
final class Select
{
    private $fields;
    private $alias;
    private $fetchArray;

    /**
     * Select constructor.
     * @param array $fields
     * @param string $alias
     * @param bool $fetchArray
     */
    public function __construct(
        array $fields = [],
        string $alias = 'o',
        bool $fetchArray = false
    ) {
        $this->fields = $fields;
        $this->alias = $alias;
        $this->fetchArray = $fetchArray;
    }

    public function getFields(): ?string
    {
        $alias = $this->alias;

        if (!$this->fetchArray) {
            return $alias;
        }

        return implode(
            ',',
            array_map(static function ($field) use ($alias) {
                return "$alias.$field";
            }, $this->fields)
        );
    }

    public function getFetchArray(): ?bool
    {
        return $this->fetchArray;
    }
}
