<?php

namespace Seat\Shared\Domain\Error;

use Seat\Shared\Domain\Message;

/**
 * Class Notification
 * @package Seat\Shared\Domain\Error
 */
final class Notification
{
    private $errors = [];
    private $responses = [];

    public function addError(string $fieldName, string $error)
    {
        $this->errors[] = new Error($fieldName, $error);

        return $this;
    }

    public function addSuccess()
    {
        $this->responses[] = new Message();

        return $this;
    }

    /**
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasError()
    {
        return \count($this->errors) > 0;
    }
}
