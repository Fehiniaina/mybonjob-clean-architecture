<?php

namespace Seat\Shared\Domain\Model;

/**
 * Class BaseViewModel
 * @package Seat\Shared\Domain\Model
 */
abstract class BaseViewModel
{
    public $notifications = [];

    public function addNotification(string $type, string $message)
    {
        $this->notifications[] = ['type' => $type, 'message' => $message];
    }
}
