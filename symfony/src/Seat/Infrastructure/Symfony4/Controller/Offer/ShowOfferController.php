<?php

namespace Symfony4\Controller\Offer;

use Seat\Api\Offer\Application\ShowOffer\Collections\ShowOfferQuery;
use Seat\Shared\Domain\Bus\BusHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony4\Controller\Offer\InputRequest\OfferGetCollectionInputData;

/**
 * Class ShowOfferController
 * @Route("/offers", name="get.offer", methods={"GET"})
 * @package Seat\Infrastructure\Symfony4\Controller\Offer
 */
final class ShowOfferController
{
    /**
     * @param OfferGetCollectionInputData $request
     * @param BusHandler $busHandler
     * @return JsonResponse
     */
    public function __invoke(
        OfferGetCollectionInputData $request,
        BusHandler $busHandler
    ): JsonResponse {
        $response = $busHandler->dispatch(
            new ShowOfferQuery(
                $request->getPage(),
                $request->getSize(),
                $request->getFields(),
                $request->getFilters(),
                $request->getSort()
            )
        );

        return new JsonResponse($response->toJson(), Response::HTTP_OK, [], true);
    }
}
