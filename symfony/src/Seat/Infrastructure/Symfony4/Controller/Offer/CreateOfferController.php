<?php

namespace Symfony4\Controller\Offer;

use Seat\Api\Offer\Application\AddOffer\AddOfferRequest;
use Seat\Shared\Domain\Bus\BusHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreateOfferController
 * @Route("/offers", name="create.offer", methods={"POST"})
 * @ParamConverter("request",
 *     converter="request_body_converter",
 *     class="Seat\Api\Offer\Application\AddOffer\AddOfferRequest")
 * @package Symfony4\Controller\Offer
 */
final class CreateOfferController
{
    /**
     * @param AddOfferRequest $request
     * @param BusHandler $busHandler
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(
        AddOfferRequest $request,
        BusHandler $busHandler
    ) {
        $busHandler->dispatch($request);

        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
