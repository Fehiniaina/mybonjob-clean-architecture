<?php

namespace Symfony4\Controller\Offer;

use Seat\Shared\Domain\Bus\BusHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony4\Controller\Offer\InputRequest\OfferGetInputData;

/**
 * Class ShowOfferByUuidController
 * @Route("/offers/{uuid}", name="get.offer.byuuid", methods={"GET"})
 * @package Symfony4\Controller\Offer
 */
final class ShowOfferByUuidController
{
    public function __invoke(
        OfferGetInputData $inputData,
        BusHandler $busHandler
    ): JsonResponse {

    }
}
