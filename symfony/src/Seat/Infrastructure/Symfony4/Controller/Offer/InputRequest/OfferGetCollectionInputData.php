<?php

namespace Symfony4\Controller\Offer\InputRequest;

use Seat\Api\Offer\Domain\OfferAssembler;
use Symfony\Component\HttpFoundation\Request;
use Symfony4\Request\InputDataRequest;

/**
 * Class OfferGetCollectionInputData
 * @package Symfony4\Controller\Offer\InputRequest
 */
final class OfferGetCollectionInputData extends InputDataRequest
{
    private $defaultPage = 1;
    private $defaultSize = 20;
    private $defaultSort = 'title,description';
    private $defaultFields = 'id,title,description';
    private $defaultFilters = ['id'];

    private $page;
    private $size;
    private $sort;
    private $fields;
    private $filters;

    public function getPage(): int
    {
        return $this->page;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return mixed[]
     */
    public function getSort(): array
    {
        return $this->sort;
    }

    /**
     * @return mixed[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return mixed[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    protected function extractAndValidateData(Request $request): void
    {
        $query = $request->query->all();

        $page = $query['page'] ?? [];
        $sort = $query['sort'] ?? '';
        $fields = $query['fields'] ?? [];
        $filter = $query['filter'] ?? [];

        $number = empty($page['number']) ? $this->defaultPage : $page['number'];
        $number = (int) ($number ?? $this->defaultPage);

        $size = empty($page['size']) ? $this->defaultSize : $page['size'];
        $size = (int) ($size ?? $this->defaultSize);

        $sort = empty($sort) ? $this->defaultSort : $sort;
        $sort = explode(',', $sort);

        $fields = empty($fields['offers']) ? $this->defaultFields : $fields['offers'];
        $fields = explode(',', $fields);

        // @todo filters here
        $filters = $filter ?? $this->defaultFilters;

        // @todo add violation constraint
        $this->page = $number;
        $this->size = $size;
        $this->fields = array_map(static function ($field) {
            return OfferAssembler::$jsonMappingToEntity[$field];
        }, $fields);
        $this->filters = array_map(static function ($filter) {
            return OfferAssembler::$jsonMappingToEntity[$filter];
        }, $filters);
        $this->sort = array_map(static function ($sort): string {
            $order = '';
            if (str_starts_with($sort, '-')) {
                $order = '-';
                $sort = substr($sort, 1, \strlen($sort));
            }
            return $order . OfferAssembler::$jsonMappingToEntity[$sort];
        }, $sort);
    }
}
