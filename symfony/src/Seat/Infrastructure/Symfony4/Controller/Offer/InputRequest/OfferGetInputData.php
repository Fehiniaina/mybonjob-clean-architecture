<?php

namespace Symfony4\Controller\Offer\InputRequest;

use Seat\Api\Offer\Domain\OfferId;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony4\Request\InputDataRequest;

/**
 * Class OfferGetInputData
 * @package Symfony4\Controller\Offer\InputRequest
 */
final class OfferGetInputData extends InputDataRequest
{
    /** @var string */
    private $uuid;

    public function getUuid(): string
    {
        return $this->uuid;
    }

    protected function extractAndValidateData(Request $request): void
    {
        $uuid = $request->attributes->get('_route_params')['uuid'];

        $albumConstrains = OfferId::getConstraints();
        $violations = Validation::createValidator()->validate($uuid, $albumConstrains);

        if ($violations->count() === 0) {
            $this->uuid = $uuid;
        }
    }
}
