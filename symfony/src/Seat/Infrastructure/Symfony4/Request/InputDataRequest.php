<?php

namespace Symfony4\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class InputDataRequest
 * @package Symfony4\Request
 */
abstract class InputDataRequest
{
    protected $errors = [];
    protected $isValid;
    private $request;

    /**
     * InputDataRequest constructor.
     * @param RequestStack $request
     */
    public function __construct(RequestStack $request)
    {
        $this->request = $request->getMainRequest();
        $this->extractAndValidateData($this->request);
    }

    /**
     * @return Request|null
     */
    public function getRequest(): ?Request
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    /**
     * @return mixed[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    abstract protected function extractAndValidateData(Request $request): void;
}
