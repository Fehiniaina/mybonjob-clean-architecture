<?php

namespace Seat\Api\Offer\Domain;

use Seat\Api\Offer\Domain\Entities\Model\OfferModel;

/**
 * Class OfferResultSet
 * @package Seat\Api\Offer\Domain
 */
final class OfferResultSet
{
    private $offers;
    private $total;

    /**
     * OfferResultSet constructor.
     * @param $offers
     * @param $total
     */
    public function __construct($offers, $total)
    {
        $this->offers = $offers;
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getOffers(): array
    {
        $offers = [];
        foreach ($this->offers as $offer) {
            if ($offer instanceof OfferModel) {
                $offers[] = OfferAssembler::fromEntityToResponse($offer);
            } else {
                $offers[] = OfferAssembler::fromArrayToResponse($offer);
            }
        }

        return $this->offers;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
