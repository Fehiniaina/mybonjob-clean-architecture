<?php

namespace Seat\Api\Offer\Domain;

use Seat\Api\Offer\Domain\Entities\Model\OfferModel;

/**
 * Class OfferAssembler
 * @package Seat\Api\Offer\Domain
 */
final class OfferAssembler
{
    public static $jsonMappingToEntity = [
        'id' => OfferModel::ID,
        'title' => OfferModel::TITLE,
        'description' => OfferModel::DESCRIPTION,
        'status' => OfferModel::STATUS,
    ];

    public static function fromEntityToArray(?OfferModel $offer): array
    {
        return (null !== $offer) ? [
            OfferModel::ID => $offer->id(),
            OfferModel::TITLE => $offer->title(),
            OfferModel::DESCRIPTION => $offer->description(),
            OfferModel::STATUS => $offer->status(),
        ] : [];
    }

    public static function fromArrayToResponse(array $offerArray): array
    {
        $result = [];

        if (isset($offerArray[OfferModel::ID])) {
            $result['id'] = $offerArray[OfferModel::ID];
        }
        if (isset($offerArray[OfferModel::TITLE])) {
            $result['title'] = $offerArray[OfferModel::TITLE];
        }
        if (isset($offerArray[OfferModel::DESCRIPTION])) {
            $result['description'] = $offerArray[OfferModel::DESCRIPTION];
        }
        if (isset($offerArray[OfferModel::STATUS])) {
            $result['status'] = $offerArray[OfferModel::STATUS];
        }

        return $result;
    }

    /**
     * @param OfferModel $offer
     * @return array<string, string>
     */
    public static function fromEntityToResponse(OfferModel $offer): array
    {
        return [
            'id' => $offer->id(),
            'title' => $offer->title(),
            'description' => $offer->description(),
            'status' => $offer->status(),
        ];
    }
}
