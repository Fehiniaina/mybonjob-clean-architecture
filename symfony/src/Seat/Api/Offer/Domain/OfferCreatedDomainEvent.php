<?php

namespace Seat\Api\Offer\Domain;

use Seat\Shared\Domain\Bus\Event\AsyncEvent;
use Seat\Shared\Domain\DomainEvent;

/**
 * Class OfferCreatedDomainEvent
 * @package Seat\Api\Offer\Domain
 */
final class OfferCreatedDomainEvent extends DomainEvent implements AsyncEvent
{
    private $title;
    private $description;
    private $status;

    /**
     * OfferCreatedDomainEvent constructor.
     * @param string $id
     * @param string $title
     * @param string $description
     * @param bool $status
     * @param string|null $eventId
     * @param string|null $eventDate
     * @throws \Exception
     */
    public function __construct(
        string $id,
        string $title,
        string $description,
        bool $status,
        string $eventId = null,
        string $eventDate = null
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;

        parent::__construct($id, $eventId, $eventDate);
    }

    /**
     * {@inheritdoc}
     */
    public static function fromPrimitives(
        string $aggregateId,
        array $data,
        string $eventId,
        string $eventDate
    ): DomainEvent {
        return new self(
            $aggregateId,
            $data['title'],
            $data['description'],
            $data['status'],
            $eventId,
            $eventDate
        );
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function toPrimitives(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'status' => $this->status,
        ];
    }

    /**
     * @return string
     */
    public static function eventName(): string
    {
        return 'offer.created';
    }
}
