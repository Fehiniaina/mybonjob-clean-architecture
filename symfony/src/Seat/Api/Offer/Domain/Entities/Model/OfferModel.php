<?php

namespace Seat\Api\Offer\Domain\Entities\Model;

use Seat\Api\Offer\Domain\OfferCreatedDomainEvent;
use Seat\Api\Offer\Domain\OfferId;
use Seat\Shared\Domain\Aggregate\AggregateRoot;

/**
 * Class OfferModel
 * @package Seat\Api\Offer\Domain\Entities\Model
 */
final class OfferModel extends AggregateRoot
{
    public const ID = 'id';
    public const TITLE = 'title';
    public const DESCRIPTION = 'description';
    public const STATUS = 'status';

    /** @var OfferId */
    private $id;
    private $title;
    private $description;
    private $status;

    /**
     * OfferModel constructor.
     * @param OfferId $id
     * @param string $title
     * @param string $description
     * @param bool $status
     */
    private function __construct(
        OfferId $id,
        string $title,
        string $description,
        ?bool $status = null
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->status = $status ?? false;
    }

    /**
     * @param OfferId $id
     * @param string $title
     * @param string $description
     * @param $status
     * @return OfferModel
     * @throws \Exception
     */
    public static function create(
        OfferId $id,
        string $title,
        string $description,
        ?bool $status = null
    ): OfferModel {
        $offer = new self($id, $title, $description, $status);

        $offer->collectEvent(
            new OfferCreatedDomainEvent(
                $id->value(),
                $title,
                $description,
                $status
            )
        );

        return $offer;
    }

    public function id(): OfferId
    {
        return $this->id;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function status(): bool
    {
        return $this->status;
    }

    public static function getFieldsName(): array
    {
        return [
            self::ID,
            self::TITLE,
            self::DESCRIPTION,
            self::STATUS,
        ];
    }

    /**
     * @param OfferModel $object
     * @return bool
     */
    public function equals($object): bool
    {
        return
            $this->id === $object->id() &&
            $this->title === $object->title() &&
            $this->description === $object->description() &&
            $this->status === $object->status();
    }
}
