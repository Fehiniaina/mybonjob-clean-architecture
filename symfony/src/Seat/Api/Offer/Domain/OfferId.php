<?php

namespace Seat\Api\Offer\Domain;

use Seat\Shared\Domain\ValueObject\UuidValueObject;

/**
 * Class OfferId
 * @package Seat\Api\Offer\Domain
 */
final class OfferId extends UuidValueObject
{
    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->toRfc4122();
    }
}
