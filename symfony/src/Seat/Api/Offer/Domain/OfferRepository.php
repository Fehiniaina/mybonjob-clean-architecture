<?php

namespace Seat\Api\Offer\Domain;

use Seat\Shared\Application\Criteria;
use Seat\Shared\Domain\Aggregate\AggregateRoot;
use Seat\Shared\Domain\Select;

interface OfferRepository
{
    /**
     * @param AggregateRoot $aggregate
     */
    public function add(AggregateRoot $aggregate): void;

    /**
     * @param Select $select
     * @param Criteria $criteria
     * @return OfferResultSet
     */
    public function getMatching(Select $select, Criteria $criteria): OfferResultSet;
}
