<?php

namespace Seat\Api\Offer\Application\AddOffer;

use Seat\Api\Offer\Domain\OfferId;
use Seat\Shared\Domain\Bus\Command\Command;

/**
 * Class AddOfferRequest
 * @package Seat\Api\Offer\Application\AddOffer
 */
final class AddOfferRequest implements Command
{
    public $id;
    public $title;
    public $description;
    public $status;

    /**
     * @param string $title
     * @param string $description
     * @param string $status
     * @param OfferId|null $id
     * @return AddOfferRequest
     */
    public static function fromAll(
        string $title,
        string $description,
        ?string $status = null,
        ?OfferId $id = null
    ): AddOfferRequest {
        $request = new self();
        $request->id = $id;
        $request->title = $title;
        $request->description = $description;
        $request->status = $status ?? false;

        return $request;
    }
}
