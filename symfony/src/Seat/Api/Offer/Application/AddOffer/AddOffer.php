<?php

namespace Seat\Api\Offer\Application\AddOffer;

use Seat\Api\Offer\Domain\Entities\Model\OfferModel;
use Seat\Api\Offer\Domain\OfferId;
use Seat\Api\Offer\Domain\OfferRepository;
use Seat\Shared\Domain\Bus\Event\EventPublisher;
use Seat\Shared\Service\ApplicationService;
use Seat\Shared\Service\Request;
use Seat\Shared\Service\Response;

/**
 * Class AddOffer
 * @package Seat\Api\Offer\Application\AddOffer
 */
final class AddOffer implements ApplicationService
{
    /** @var OfferRepository */
    private $repository;

    /** @var EventPublisher */
    private $publisher;

    /**
     * AddOffer constructor.
     * @param OfferRepository $repository
     * @param EventPublisher $publisher
     */
    public function __construct(
        OfferRepository $repository,
        EventPublisher $publisher
    ) {
        $this->repository = $repository;
        $this->publisher = $publisher;
    }

    /**
     * @param Request $request
     * @return Response|null
     * @throws \Exception
     */
    public function execute(Request $request): ?Response
    {
        $offer = OfferModel::create(
            $request->id ?? OfferId::random(),
            $request->title,
            $request->description,
            $request->status
        );

        $this->repository->add($offer);
        $this->publisher->publish(...$offer->dropEvents());

        return null;
    }
}
