<?php

namespace Seat\Api\Offer\Application\AddOffer;

use Psr\Log\LoggerInterface;
use Seat\Api\Offer\Domain\OfferCreatedDomainEvent;
use Seat\Shared\Domain\Bus\Event\EventHandler;

/**
 * Class OfferCreatedListener
 * @package Seat\Api\Offer\Application\AddOffer
 */
final class OfferCreatedListener implements EventHandler
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * OfferCreatedListener constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute(OfferCreatedDomainEvent $event)
    {
        $this->logger->info('Async OfferCreatedDomainEvent message received!');
    }
}
