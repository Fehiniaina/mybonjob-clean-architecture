<?php

namespace Seat\Api\Offer\Application\ShowOffer\Get;

use Seat\Api\Offer\Domain\OfferId;
use Seat\Shared\Domain\Bus\Query\SyncQuery;

/**
 * Class ShowOfferByUuidQuery
 * @package Seat\Api\Offer\Application\ShowOffer\Get
 */
final class ShowOfferByUuidQuery implements SyncQuery
{
    /** @var OfferId */
    private $uuid;

    /**
     * ShowOfferByUuidQuery constructor.
     * @param OfferId $uuid
     */
    public function __construct(OfferId $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return OfferId
     */
    public function getUuid(): OfferId
    {
        return $this->uuid;
    }
}
