<?php

namespace Seat\Api\Offer\Application\ShowOffer\Get;

use Seat\Shared\Service\Response;

/**
 * Class ShowOfferByUuidResponse
 * @package Seat\Api\Offer\Application\ShowOffer\Get
 */
final class ShowOfferByUuidResponse implements Response
{
    /** @var array */
    private $offer = [];

    /**
     * ShowOfferByUuidResponse constructor.
     * @param array $offer
     */
    public function __construct(array $offer)
    {
        $this->offer = $offer;
    }

    public function getTotal(): int
    {
        return \count($this->offer);
    }

    public function toJson(): string
    {
        return json_encode(
            [
                'data' => $this->offer,
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
