<?php

namespace Seat\Api\Offer\Application\ShowOffer\Collections;

use Seat\Shared\Domain\Bus\Query\SyncQuery;

/**
 * Class ShowOfferQuery
 * @package Seat\Api\Offer\Application\ShowOffer\Collections
 */
final class ShowOfferQuery implements SyncQuery
{
    private $page;
    private $size;
    private $fields = [];
    private $filters = [];
    private $sort = [];

    /**
     * ShowOfferQuery constructor.
     * @param $page
     * @param $size
     * @param array $fields
     * @param array $filters
     * @param array $sort
     */
    public function __construct(
        int $page,
        int $size,
        array $fields,
        array $filters,
        array $sort
    ) {
        $this->page = $page;
        $this->size = $size;
        $this->fields = $fields;
        $this->filters = $filters;
        $this->sort = $sort;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return string[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return string[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return string[]
     */
    public function getSort(): array
    {
        return $this->sort;
    }
}
