<?php

namespace Seat\Api\Offer\Application\ShowOffer\Collections;

use Seat\Shared\Service\Response;

/**
 * Class ShowOfferResponse
 * @package Seat\Api\Offer\Application\ShowOffer\Collections
 */
final class ShowOfferResponse implements Response
{
    private $offers;
    private $total;
    private $page;
    private $size;

    /**
     * ShowOfferResponse constructor.
     * @param $offers
     * @param $total
     * @param $page
     * @param $size
     */
    public function __construct($offers, $total, $page, $size)
    {
        $this->offers = $offers;
        $this->total = $total;
        $this->page = $page;
        $this->size = $size;
    }

    public function toJson(): string
    {
        return json_encode(
            [
                'data' => $this->getData(),
                'links' => $this->getLinks(),
                'meta' => $this->getMeta(),
            ],
            JSON_THROW_ON_ERROR
        );
    }

    /**
     * @return mixed[]
     */
    private function getData(): array
    {
        return $this->offers;
    }

    /**
     * @return array<string, string>
     */
    private function getLinks(): array
    {
        if ($this->total === 0) {
            return [];
        }
        $self = $this->page;
        $size = $this->size;
        $last = ceil($this->total / $this->size);
        $first = 1;
        $prev = ceil(($self - 1 <= $first) ? $first : $self - 1);
        $next = ceil(($self + 1 >= $last) ? $last : $self + 1);

        $url = str_replace(['[', ']'], ['%%5B', '%%5D'], '/offers?page[number]=%s&page[size]=%s');

        return [
            'self' => sprintf($url, $self, $size),
            'first' => sprintf($url, $first, $size),
            'prev' => sprintf($url, $prev, $size),
            'next' => sprintf($url, $next, $size),
            'last' => sprintf($url, $last, $size),
        ];
    }

    /**
     * @return  array<string, float>
     */
    private function getMeta(): array
    {
        return [
            'total_pages' => ceil($this->total / $this->size),
        ];
    }
}
