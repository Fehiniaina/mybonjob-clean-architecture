<?php

namespace Seat\Api\Offer\Application\ShowOffer;

use Seat\Api\Offer\Application\ShowOffer\Collections\ShowOfferResponse;
use Seat\Api\Offer\Domain\OfferRepository;
use Seat\Shared\Application\Criteria;
use Seat\Shared\Domain\Select;
use Seat\Shared\Service\ApplicationService;
use Seat\Shared\Service\Request;
use Seat\Shared\Service\Response;

/**
 * Class ShowOffer
 * @package Seat\Api\Offer\Application\ShowOffer\Collections
 */
final class ShowOffer implements ApplicationService
{
    /** @var OfferRepository */
    private $repository;

    /**
     * ShowOffer constructor.
     * @param OfferRepository $repository
     */
    public function __construct(OfferRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response|null
     */
    public function execute(Request $request): ?Response
    {
        $alias = 'offer';
        $criteria = new Criteria(
            $request->getFilters(),
            $request->getSort(),
            $request->getPage(),
            $request->getSize(),
            $alias
        );
        $select = new Select(
            $request->getFields(),
            $alias,
            true
        );

        $resultSet = $this->repository->getMatching($select, $criteria);

        return new ShowOfferResponse(
            $resultSet->getOffers(),
            $resultSet->getTotal(),
            $request->getPage(),
            $request->getSize()
        );
    }
}
