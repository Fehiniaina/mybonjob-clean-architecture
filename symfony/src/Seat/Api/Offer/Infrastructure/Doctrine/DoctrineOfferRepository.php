<?php

namespace Seat\Api\Offer\Infrastructure\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Seat\Api\Offer\Domain\OfferRepository;
use Seat\Api\Offer\Domain\OfferResultSet;
use Seat\Shared\Application\Criteria;
use Seat\Shared\Domain\Aggregate\AggregateRoot;
use Seat\Shared\Domain\Select;
use Symfony4\Entity\Offer;

/**
 * Class DoctrineOfferRepository
 * @package Seat\Api\Offer\Infrastructure\Doctrine
 */
class DoctrineOfferRepository extends ServiceEntityRepository implements OfferRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    public function add(AggregateRoot $aggregate): void
    {
        $offer = new Offer();
        $offer->setId($aggregate->id());
        $offer->setTitle($aggregate->title());
        $offer->setDescription($aggregate->description());
        $offer->setStatus($aggregate->status());
        $this->getEntityManager()->persist($offer);
        $this->getEntityManager()->flush();
    }

    public function getMatching(Select $select, Criteria $criteria): OfferResultSet
    {
        $offers = $this->createQueryBuilder($criteria->getAlias())
            ->select($select->getFields())
            ->addCriteria($criteria->getCriteria())
            ->getQuery()
            ->execute();

        $total = $this->count([]);

        return new OfferResultSet($offers, $total);
    }
}
